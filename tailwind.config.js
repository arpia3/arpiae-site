const colors = require('tailwindcss/colors')

module.exports = {
  content: ["./html/*.html", "./html/assets/js/*.js"],
  theme: {
    colors: {
      transparent: 'transparent',
      current: 'currentColor',
      'white': '#fefefe',
      'ar-blue': {
        400: '#00A3FF',
        500: '#1983FF',
        600: '#3262FF',
        700: '#2D59E8',
        800: '#2951D3',
        900: '#2548BB'
      },
      'ar-neutral': {
        50: '#fafafa',
        100: '#F2F2F2',
        200: '#E0E0E0',
        400: '#898888',
        500: '#545353',
        700: '#3A3839',
        900: '#1F1D1E'
      },
      'neutral': colors.neutral,
      'slate': colors.slate
    },
    extend: {
      transitionProperty: {
        'decoration': 'text-decoration-color'
      },
      boxShadow: {
        'thiccc': '0 8px 15px -3px rgb(0, 0, 0, 0.3)',
        "center-thic": '0px 0px 14px 0px rgba(0,0,0,0.4)',
        "inner-thic": 'inset 0px 0px 14px 0px rgba(0,0,0,0.4)'
      },

    },
  },
  plugins: [],
}
