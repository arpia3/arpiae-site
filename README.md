# Site ARPIAE

## Dev

### Tailwind

#### Installation

Installer le module TailwindCSS pour la compilation du CSS en direct

```sh
npm install
```

#### Lancé la compilation en direct

```sh
npx tailwindcss -i ./tailwind.css -o ./html/assets/css/tailwind.css --watch
```
